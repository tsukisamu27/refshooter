import pygame
from pygame.locals import *
from setting import *

class MapFlame:
    def __init__(self):
        self.margin = 10
        self.width = WIDTH-20
        self.height = HEIGHT-20

    def draw(self, screen):
        pygame.draw.rect(screen, BLUE, Rect(self.margin, self.margin, self.width, self.height) ,2)
    
    def getCorners(self):
        corners = [ (self.margin, self.margin), #左上
                    (WIDTH-self.margin, self.margin), #右上
                    (WIDTH-self.margin, HEIGHT-self.margin),#右下 
                    (self.margin, HEIGHT-self.margin)] #左下
        return corners

    def getCenter(self):
        x = self.width / 2
        y = self.height / 2
        return x, y

