import numpy as np
import pygame
import math

##壁にぶつかる座標を算出
def calculate(start_x, start_y, mouse_x, mouse_y, corners):
    #corners = mf.getCorners()

    # 自機から見たそれぞれの角の偏角を計算
    corners_deg = [math.atan2(start_y - cy , cx - start_x) for (cx, cy) in corners]

    # 自機から見たマウスの位置
    mouse_deg = math.atan2(start_y - mouse_y, mouse_x - start_x)

    dx = start_x - mouse_x + 0.1
    dy = start_y - mouse_y + 0.1
    slope = dy/dx

    if corners_deg[0] >= mouse_deg and mouse_deg > corners_deg[1]:
        # 上跳ね返り
        x_new = (10 - start_y ) / slope + start_x
        y_new = 10

    elif corners_deg[1] >= mouse_deg and mouse_deg > corners_deg[2]:
        # 右跳ね返り
        x_new = 470
        y_new = slope * (x_new - start_x) + start_y

    elif corners_deg[2] >= mouse_deg and mouse_deg > corners_deg[3]:
        # 下跳ね返り
        x_new = (350 - start_y) / slope +start_x
        y_new = 350

    else:
        # 左跳ね返り
        x_new = 10
        y_new = slope * (x_new - start_x) + start_y

    return x_new, y_new

# 法線
def calcLineNormal(begin_x, end_x, begin_y, end_y):
    dirVec = pygame.math.Vector2(begin_x - end_x, begin_y - end_y);
    normal = pygame.math.Vector2(-dirVec.y, dirVec.x);
    normarizeNomal = normarize(normal.x,normal.y)
    return normarizeNomal ,dirVec

# マップフレームから法線を算出
def housen(start_x, start_y, end_x, end_y, corners):
    #corners = mf.getCorners()

    # 自機から見たそれぞれの角の偏角を計算
    corners_deg = [math.atan2(start_y - cy , cx - start_x) for (cx, cy) in corners]
    # 自機から見たマウスの位置
    mouse_deg = math.atan2(start_y - end_y, end_x - start_x)

    if corners_deg[0] >= mouse_deg and mouse_deg > corners_deg[1]:
        # 上跳ね返り
        normarizeNomal,dirVec  = calcLineNormal(corners[0][0],corners[1][0],corners[0][1],corners[1][1])

    elif corners_deg[1] >= mouse_deg and mouse_deg > corners_deg[2]:
        # 右跳ね返り
        normarizeNomal,dirVec = calcLineNormal(corners[1][0],corners[2][0],corners[1][1],corners[2][1])

    elif corners_deg[2] >= mouse_deg and mouse_deg > corners_deg[3]:
        # 下跳ね返り
        normarizeNomal,dirVec = calcLineNormal(corners[2][0],corners[3][0],corners[2][1],corners[3][1])

    else:
        # 左跳ね返り
        normarizeNomal,dirVec = calcLineNormal(corners[3][0],corners[0][0],corners[3][1],corners[0][1])

    #正しい法線nは、玉の方向ベクトルと鈍角をなす。鈍角をなす２つのベクトルは内積を取ると負の値を取るので、以下の処理を挟んで正しい法線ベクトルを得る必要がある。
    directionVec = pygame.math.Vector2(end_x - start_x , end_y - start_y)
    if np.dot(directionVec,normarizeNomal) > 0:
        normarizeNomal = -normarizeNomal
    
    return normarizeNomal

def normarize(x,y):
    a = np.array([x,y])
    b = a / np.linalg.norm(a) 
    return b

#入射と法線を入れて反射ベクトルを出力
def reflectVec(direction_x, direction_y, nomal_x, nomal_y):
    directionVec = pygame.math.Vector2(direction_x, direction_y)
    nomalVec = pygame.math.Vector2(nomal_x, nomal_y)

    L = -normarize(directionVec.x,directionVec.y)
    nomal = np.array([nomalVec.x,nomalVec.y])
    LdotNx2 = 2.0 * np.dot(L,nomal)
    r = LdotNx2 * nomal - L 
    return normarize(r[0],r[1])
