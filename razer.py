import pygame
from pygame.locals import *
from setting import *
import numpy as np
from math import *
from reflectmath import * 
import random
from pygame.math import Vector2

class linePoint():
    def __init__(self, position_x, position_y, move_x, move_y, speed):
        self.position = Vector2(position_x, position_y)
        self.speed = speed
        self.setDirection(move_x, move_y)
    
    def setDirection(self, move_x, move_y):
        move_x, move_y = self.normarizeMove(move_x, move_y)
        self.direction = Vector2(move_x, move_y)

    def move(self):
        self.position.x += self.direction.x
        self.position.y += self.direction.y

    def normarizeMove(self,x,y):
        x,y = normarize(x,y)
        x = x * self.speed
        y = y * self.speed
        return x, y

class Razer:
    def __init__(self, start_x, start_y, move_x, move_y):
        self.speed = 3
        self.lasthanekaeri = 9
        self.refrectionTimes = 0
        self.colorlist = [GREEN, RED, YELLOW, BLUE]
        self.colorNum = 0
        self.pointlist = []
        self.pointlist.append(linePoint(start_x, start_y, move_x, move_y, self.speed))
        self.pointlist.append(linePoint(start_x, start_y, move_x, move_y, self.speed))
        self.lasthit = [['False', 'False'], ['False', 'False'], ['False', 'False'], ['False', 'False']]
        self.btoe = 0
        self.deletePoint = False
        self.oldhitNum = 9

    def changeColor(self):
        if len(self.colorlist) > self.colorNum + 1:
            self.colorNum += 1
        else:
            self.colorNum = 0

    def getColor(self):
        return self.colorlist[self.colorNum]
    
    def deleteCheck(self):
        return self.deletePoint

    def draw(self, screen):
        point = []
        for p in self.pointlist:
            point.append((p.position.x, p.position.y))

        pygame.draw.lines(screen, self.getColor(), False, point, 2)

    def DuringTrueContains(self, checklist):
        for i in checklist:
            if i == 'True':
                return True
        return False

    def diffTrueList(self, checklist1, checklist2):
        indexlist = []
        for i in range(len(checklist1)):
            if checklist1[i] != checklist2[i] and checklist1[i] == True:
                indexlist.append(i)
        return indexlist

    def move(self, WIDTH, HEIGHT):
        # 2点の距離を求める
        startArray   = np.array([self.pointlist[0].position.x, self.pointlist[0].position.y])
        betweenArray = np.array([self.pointlist[-2].position.x, self.pointlist[-2].position.y])
        endArray     = np.array([self.pointlist[-1].position.x, self.pointlist[-1].position.y])
        stoe = startArray   - endArray
        btoe = betweenArray - endArray
        self.btoe = btoe
        
        ue = ['True' if c.position.y < 13 else 'False' for c in self.pointlist]
        migi = ['True' if c.position.x > WIDTH-11 else 'False' for c in self.pointlist]
        sita = ['True' if c.position.y > HEIGHT-11 else 'False' for c in self.pointlist]
        hidari = ['True' if c.position.x < 13 else 'False' for c in self.pointlist]

        # 前回と比較し最新のCollisionをもとに処理する。
        newHit = [ue, migi, sita, hidari]
        oldHit = self.lasthit
        newHitSimple = []
        oldHitSimple = []
        hitNum = 9

        # リストをシンプル化
        for hit in newHit:
            newHitSimple.append(self.DuringTrueContains(hit))

        for hit in oldHit:
            oldHitSimple.append(self.DuringTrueContains(hit))

        # 前回と比較
        diffindex = self.diffTrueList(newHitSimple, oldHitSimple)

        if not diffindex:
            hitNum = self.oldhitNum
        else:
            hitNum = diffindex[0]
            self.oldhitNum = hitNum
        
        # 次回のために記録
        self.lasthit = newHit
        
        if hitNum == 0:# 上跳ね返り
            self.refrectMove(0,1,1,btoe)
        
        elif hitNum == 1:# 右跳ね返り
            self.refrectMove(-1,-0,2,btoe)
        
        elif hitNum == 2:# 下跳ね返り
            self.refrectMove(0,-1,3,btoe)

        elif hitNum == 3:# 左跳ね返り
            self.refrectMove(1,-0,4,btoe)

        else:
            if np.linalg.norm(stoe) <= 50:
                self.pointlist[0].move()

            elif np.linalg.norm(stoe) > 50:
                self.pointlist[0].move()
                self.pointlist[-1].move()

    def refrectMove(self,housen_x,housen_y,lasthanekaeri,btoe):

        if self.lasthanekaeri != lasthanekaeri:
            move = self.pointlist[0].direction
            # 反射ベクトルを計算しpointを追加
            ref_x, ref_y = reflectVec(move.x, move.y ,housen_x, housen_y)
            start = self.pointlist[0].position
            self.pointlist.insert(1,linePoint(start.x, start.y,  ref_x, ref_y, self.speed))
            # startpointにも新しいdirectionを設定
            self.pointlist[0].setDirection(ref_x, ref_y)

            # 跳ね返りした壁を記憶
            self.lasthanekaeri = lasthanekaeri

            if self.refrectionTimes < 3:
                self.changeColor()

            self.refrectionTimes += 1
        
        # 最後のリフレクトなら
        elif self.refrectionTimes > 3:
            self.pointlist[-1].move()
            if np.linalg.norm(btoe) <= 0:
                self.deletePoint = True

        elif np.linalg.norm(btoe) > 0:
            self.pointlist[0].move()
            self.pointlist[-1].move()

        else:
            #削除するポイントのdirectionをendpointに受け継ぐ
            self.pointlist[-1].direction = self.pointlist[-2].direction
            
            #最後から２番目のポイントを削除
            self.pointlist.pop(-2)

            self.pointlist[0].move()
            self.pointlist[-1].move()