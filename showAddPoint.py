import pygame
from pygame.locals import *
from setting import *

class ShowAddPoint:
    def __init__(self, left, top, seconds:int, addPoint:int):
        self.left = left
        self.top = top
        self.addPoint = str(addPoint)
        self.font = pygame.font.SysFont(None, 30)
        self.totitle = self.font.render("+"+ self.addPoint, True, WHITE)
        self.setTimer(seconds)

    def draw(self, screen):
        screen.blit(self.totitle, (self.left, self.top))
        self.timer -= 1

    def deleteCheck(self):
        if self.timer < 0:
            return True
        else:
            return False

    def setTimer(self, seconds):
        self.timer = seconds * FPS