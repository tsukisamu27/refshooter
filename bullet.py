import pygame
from pygame.locals import *
from setting import *

class Bullet:
    def __init__(self):
        self.left = random.randint(10, WIDTH-20)
        self.top = random.randint(10, HEIGHT-20)
        self.move_x = random.uniform(-1.0, 1.0)
        self.move_y = random.uniform(-1.0, 1.0)
        self.width = 10
        self.height = 10

    def draw(self, screen):
        self.Rect = Rect(self.left, self.top, self.width, self.height)
        pygame.draw.rect(screen, WHITE, self.Rect, 2)

    def move(self):
        self.left += self.move_x
        self.top  += self.move_y
        
        if self.top < 10:
            # 上跳ね返り
            self.move_x, self.move_y = reflectVec(self.move_x, self.move_y ,0, 1)
            self.top = 10

        elif self.left > WIDTH-10 - self.width:
            # 右跳ね返り
            self.move_x, self.move_y = reflectVec(self.move_x, self.move_y ,-1, -0)
            self.left = WIDTH-10 - self.width

        elif self.top > HEIGHT-10 - self.width:
             # 下跳ね返り
            self.move_x, self.move_y = reflectVec(self.move_x, self.move_y ,0, -1)
            self.top = HEIGHT-10 - self.width

        elif self.left < 10 :
            # 左跳ね返り
            self.move_x, self.move_y = reflectVec(self.move_x, self.move_y ,1, -0)
            self.left = 10 
        else:
            pass

    def deleteCheck(self, list):
        if self.top <10:
            bulletlist.remove(self)